package fr.bopol.grinchers;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {


    EditText edtHR, editTextGrinch;
    Button btnCopyHR, btnCopyGrinch, clear, btnHRtoGrinch, btnGrinchToHR;

    ClipboardManager clipboardManager;
    ClipData clipData;

    SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPref = new SharedPref(this);
        if (sharedPref.loadNightModeState() == true) {
            setTheme(R.style.darktheme);
        } else {
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.HomeToolbarTitle));

        edtHR = (EditText) findViewById(R.id.textarea_HR);
        editTextGrinch = (EditText) findViewById(R.id.textarea_Grinch);

        btnCopyHR = (Button) findViewById(R.id.copy_hr_to_clipboard);
        btnCopyGrinch = (Button) findViewById(R.id.copy_grinch_to_clipboard);
        btnHRtoGrinch = (Button) findViewById(R.id.to_grinch_button);
        btnGrinchToHR = (Button) findViewById(R.id.to_hr_button);

        clear = (Button) findViewById(R.id.copy_grinch_to_clipboard);

        clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        btnCopyHR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = edtHR.getText().toString();
                if (text.isEmpty()) {
                    Toast.makeText(getApplicationContext(), getString(R.string.empty_text), Toast.LENGTH_SHORT).show();
                } else {
                    clipData = clipData.newPlainText("text", text);
                    clipboardManager.setPrimaryClip(clipData);
                    Toast.makeText(getApplicationContext(), getString(R.string.copied_to_clipboard), Toast.LENGTH_SHORT).show();

                }
            }
        });

        btnCopyGrinch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editTextGrinch.getText().toString();
                if (text.isEmpty()) {
                    Toast.makeText(getApplicationContext(), getString(R.string.empty_text), Toast.LENGTH_SHORT).show();
                } else {
                    clipData = clipData.newPlainText("text", text);
                    clipboardManager.setPrimaryClip(clipData);
                    Toast.makeText(getApplicationContext(), getString(R.string.copied_to_clipboard), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnHRtoGrinch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String hrText = edtHR.getText().toString();
                String grinchIzed = Converter.grinch(hrText);
                editTextGrinch.setText(grinchIzed);
            }
        });

        btnGrinchToHR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String grinchText = editTextGrinch.getText().toString();
                String unGrinchIzed = Converter.ungrinch(grinchText);
                edtHR.setText(unGrinchIzed);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clear:
                String textHR = edtHR.getText().toString();
                String textGrinch = editTextGrinch.getText().toString();
                if (textHR.isEmpty() && textGrinch.isEmpty()) {
                    Toast.makeText(getApplicationContext(), getString(R.string.already_empty), Toast.LENGTH_SHORT).show();
                } else {
                    edtHR.setText("");
                    editTextGrinch.setText("");
                }
                return true;
            case R.id.settings:
                Intent settingsActivity = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(settingsActivity);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
