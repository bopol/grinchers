package fr.bopol.grinchers;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {
    SharedPreferences mySharedPref;

    public SharedPref(Context context) {
        mySharedPref = context.getSharedPreferences("filename", Context.MODE_PRIVATE);
    }

    public void setNightModeState(boolean state) {
        //this method saves the NigthMode State : true or false
        SharedPreferences.Editor editor = mySharedPref.edit();
        editor.putBoolean("NightMode", state);
        editor.commit();
    }

    public boolean loadNightModeState() {
        //this method loads the Night Mode State
        boolean state = mySharedPref.getBoolean("NightMode", false);
        return state;
    }
}
