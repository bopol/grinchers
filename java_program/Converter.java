import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Converter {

    public static String grinch(String input) {
        return salt(convert(input, getLookupTable()));
    }

    public static String ungrinch(String input) {
        return unconvert(unsalt(input), getReversedLookupTable());
    }

    public static String convert(String inputString, Map lookUpTable) {
        String ouputString = "";
        String input = inputString.toLowerCase();
        for (int i = 0; i < input.length(); i++) {
            ouputString += lookUpTable.get(String.valueOf(input.charAt(i)));
        }
        return ouputString;
    }

    public static String unconvert(String needsUnconvert, Map reversedLookupTable) {
        String out = "";
        String[] str;
        str = needsUnconvert.split(" ");
        for (int i = 0; i < str.length; i++) {
            out += reversedLookupTable.get(String.valueOf(str[i] + " "));
        }
        return out;
    }

    public static Map getLookupTable() {
        Map<String, String> lookUpTable = new HashMap<>();
        lookUpTable.put("a", "rg ");
        lookUpTable.put("b", "grrr ");
        lookUpTable.put("c", "grgr ");
        lookUpTable.put("d", "grr ");
        lookUpTable.put("e", "r ");
        lookUpTable.put("f", "rrgr ");
        lookUpTable.put("g", "ggr ");
        lookUpTable.put("h", "rrrr ");
        lookUpTable.put("i", "rr ");
        lookUpTable.put("j", "rggg ");
        lookUpTable.put("k", "grg ");
        lookUpTable.put("l", "rgrr ");
        lookUpTable.put("m", "gg ");
        lookUpTable.put("n", "gr ");
        lookUpTable.put("o", "ggg ");
        lookUpTable.put("p", "rggr ");
        lookUpTable.put("q", "ggrg ");
        lookUpTable.put("r", "rgr ");
        lookUpTable.put("s", "rrr ");
        lookUpTable.put("t", "g ");
        lookUpTable.put("u", "rrg ");
        lookUpTable.put("v", "rrrg ");
        lookUpTable.put("w", "rgg ");
        lookUpTable.put("x", "grrg ");
        lookUpTable.put("y", "grgg ");
        lookUpTable.put("z", "ggrr ");
        lookUpTable.put("1", "rgggg ");
        lookUpTable.put("2", "rrggg ");
        lookUpTable.put("3", "rrrgg ");
        lookUpTable.put("4", "rrrrg ");
        lookUpTable.put("5", "rrrrr ");
        lookUpTable.put("6", "grrrr ");
        lookUpTable.put("7", "ggrrr ");
        lookUpTable.put("8", "gggrr ");
        lookUpTable.put("9", "ggggr ");
        lookUpTable.put("0", "ggggg ");
        lookUpTable.put(" ", " ");
        lookUpTable.put("\n", "grrg ");

        //Punctuation, short = r and long = g
        lookUpTable.put(".", "rgrgrg ");
        lookUpTable.put(",", "ggrrgg ");
        lookUpTable.put("?", "rrggrr ");
        lookUpTable.put("'", "rggggr ");
        lookUpTable.put("!", "grgrgg ");
        lookUpTable.put("/", "grrgr ");
        lookUpTable.put("(", "grggr ");
        lookUpTable.put(")", "grggrg ");
        lookUpTable.put("&", "rgrrr ");
        lookUpTable.put(":", "gggrrr ");
        lookUpTable.put(";", "grgrgr ");
        lookUpTable.put("=", "grrrg ");
        lookUpTable.put("+", "rgrgr ");
        lookUpTable.put("-", "grrrrg ");
        lookUpTable.put("_", "rrggrg ");
        lookUpTable.put("\"", "rgrrgr ");
        lookUpTable.put("$", "rrrgrrg ");
        lookUpTable.put("@", "rggrgr ");

        //Extra symbols
        lookUpTable.put("æ", "rgrg ");
        lookUpTable.put("à", "rggrg ");
        lookUpTable.put("ç", "grgrr ");
        lookUpTable.put("ð", "rrggr ");
        lookUpTable.put("è", "rgrrg ");
        lookUpTable.put("é", "rrgrr ");
        lookUpTable.put("ĝ", "ggrgr ");
        lookUpTable.put("ĥ", "gggg ");
        lookUpTable.put("ĵ", "rgggr ");
        lookUpTable.put("ñ", "ggrgg ");
        lookUpTable.put("ø", "gggr ");
        lookUpTable.put("ŝ", "rrrgr ");
        lookUpTable.put("þ", "gggrr ");
        lookUpTable.put("ŭ", "rrgg ");

        //asked characters, i took the communication characters that are useless for us
        lookUpTable.put("#", "rrrgggrrr ");
        lookUpTable.put("ê", "grg ");
        lookUpTable.put("%", "rgrg ");
        lookUpTable.put("á", "rrrrrrrr ");
        lookUpTable.put("í", "rgrrrgrr ");
        lookUpTable.put("ó", "rggrrggr ");
        lookUpTable.put("ú", "rrrrgrr ");
        lookUpTable.put("ù", "rgrgr ");

        return lookUpTable;
    }

    public static Map getReversedLookupTable() {
        Map<String, String> reversedLookupTable = new HashMap<>();
        reversedLookupTable.put("rg ", "a");
        reversedLookupTable.put("grrr ", "b");
        reversedLookupTable.put("grgr ", "c");
        reversedLookupTable.put("grr ", "d");
        reversedLookupTable.put("r ", "e");
        reversedLookupTable.put("rrgr ", "f");
        reversedLookupTable.put("ggr ", "g");
        reversedLookupTable.put("rrrr ", "h");
        reversedLookupTable.put("rr ", "i");
        reversedLookupTable.put("rggg ", "j");
        reversedLookupTable.put("grg ", "k");
        reversedLookupTable.put("rgrr ", "l");
        reversedLookupTable.put("gg ", "m");
        reversedLookupTable.put("gr ", "n");
        reversedLookupTable.put("ggg ", "o");
        reversedLookupTable.put("rggr ", "p");
        reversedLookupTable.put("ggrg ", "q");
        reversedLookupTable.put("rgr ", "r");
        reversedLookupTable.put("rrr ", "s");
        reversedLookupTable.put("g ", "t");
        reversedLookupTable.put("rrg ", "u");
        reversedLookupTable.put("rrrg ", "v");
        reversedLookupTable.put("rgg ", "w");
        reversedLookupTable.put("grrg ", "x");
        reversedLookupTable.put("grgg ", "y");
        reversedLookupTable.put("ggrr ", "z");
        reversedLookupTable.put("rgggg ", "1");
        reversedLookupTable.put("rrggg ", "2");
        reversedLookupTable.put("rrrgg ", "3");
        reversedLookupTable.put("rrrrg ", "4");
        reversedLookupTable.put("rrrrr ", "5");
        reversedLookupTable.put("grrrr ", "6");
        reversedLookupTable.put("ggrrr ", "7");
        reversedLookupTable.put("ggggr ", "8");
        reversedLookupTable.put("ggggr ", "9");
        reversedLookupTable.put("ggggg ", "0");
        reversedLookupTable.put(" ", " ");
        reversedLookupTable.put("grrg ", "\n");

        //Punctuation
        reversedLookupTable.put("rgrgrg ", ".");
        reversedLookupTable.put("ggrrgg ", ",");
        reversedLookupTable.put("rrggrr ", "?");
        reversedLookupTable.put("rggggr ", "'");
        reversedLookupTable.put("grgrgg ", "!");
        reversedLookupTable.put("grrgr ", "/");
        reversedLookupTable.put("grggr ", "(");
        reversedLookupTable.put("grggrg ", ")");
        reversedLookupTable.put("rgrrr ", "&");
        reversedLookupTable.put("gggrrr ", ":");
        reversedLookupTable.put("grgrgr ", ";");
        reversedLookupTable.put("grrrg ", "=");
        reversedLookupTable.put("rgrgr ", "+");
        reversedLookupTable.put("grrrrg ", "-");
        reversedLookupTable.put("rrggrg ", "_");
        reversedLookupTable.put("rgrrgr ", "\"");
        reversedLookupTable.put("rrrgrrg ", "$");
        reversedLookupTable.put("rggrgr ", "@");

        //Extra symbols
        reversedLookupTable.put("rgrg ", "æ");
        reversedLookupTable.put("rggrg ", "à");
        reversedLookupTable.put("grgrr ", "ç");
        reversedLookupTable.put("rrggr ", "ð");
        reversedLookupTable.put("rgrrg ", "è");
        reversedLookupTable.put("rrgrr ", "é");
        reversedLookupTable.put("ggrgr ", "ĝ");
        reversedLookupTable.put("gggg ", "ĥ");
        reversedLookupTable.put("rgggr ", "ĵ");
        reversedLookupTable.put("ggrgg ", "ñ");
        reversedLookupTable.put("gggr ", "ø");
        reversedLookupTable.put("rrrgr ", "ŝ");
        reversedLookupTable.put("gggrr ", "þ");
        reversedLookupTable.put("rrgg ", "ŭ");

        //asked characters, i took the communication characters that are useless for us
        reversedLookupTable.put("rrrgggrrr ", "#");
        reversedLookupTable.put("grg ", "ê");
        reversedLookupTable.put("rgrg ", "%");
        reversedLookupTable.put("rrrrrrrr ", "á");
        reversedLookupTable.put("rgrrrgrr ", "í");
        reversedLookupTable.put("rggrrggr ", "ó");
        reversedLookupTable.put("rrrrgrr ", "ú");
        reversedLookupTable.put("rgrgr ", "ù");

        return reversedLookupTable;
    }

    public static String salt(String string) {
        String[] inch = {"i", "n", "c", "h"};
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        String salted = "";
        String randInch;
        int random_inch;

        for (int i = 0; i < string.length(); i++) {
            salted += String.valueOf(string.charAt(i));
            for (int j = 0; j < 3; j++) {
                random_inch = random.nextInt(inch.length);
                randInch = inch[random_inch];
                if (Math.random() > 0.5) {
                    salted += randInch;
                }
            }
        }
        return salted;
    }

    public static String unsalt(String string) {
        string = string.replaceAll("i", "");
        string = string.replaceAll("n", "");
        string = string.replaceAll("c", "");
        string = string.replaceAll("h", "");
        return string;
    }
}
